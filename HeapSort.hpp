//
//  HeapSort.hpp
//  HeapSort
//
//  Created by Rumeysa Bulut on 12.11.2017.
//  Copyright © 2017 Rumeysa Bulut. All rights reserved.
//

#ifndef HeapSort_hpp
#define HeapSort_hpp
#include <vector>

using namespace std;
class HeapSort{
  private:
    vector<long> heap_array;
    long largest;
    long heap_size;
  public:
    
    void maxHeapify( long);
    void buildMaxHeap();
    void heapSort();
    void addElements(long);
    long getSize();
    long getElement(long);
};
#endif /* HeapSort_hpp */
