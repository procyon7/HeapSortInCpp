//
//  main.cpp
//  HeapSort
//
//  Created by Rumeysa Bulut on 12.11.2017.
//  Copyright © 2017 Rumeysa Bulut. All rights reserved.
//

#include <iostream>
#include <string>
#include "HeapSort.hpp"
using namespace std;
int main(int argc, const char * argv[]) {
    // insert code here...
    std::cout << "HEAP SORT\n"<< endl;
    HeapSort heapIt;
    string element = "";
    cout<< "Enter long type vector elements, press e to end element enter process:"<< endl;
    while(element != "e"){
        cin >> element;
        if(element != "e"){
            heapIt.addElements(stol(element));
        }
    }
    heapIt.heapSort();
    cout << "After sorting:"<<endl;
    for(long i = 0; i < heapIt.getSize(); i++){
        cout << heapIt.getElement(i) << ", ";
    }
    cout << endl;
    return 0;
}
