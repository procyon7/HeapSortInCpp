//
//  HeapSort.cpp
//  HeapSort
//
//  Created by Rumeysa Bulut on 12.11.2017.
//  Copyright © 2017 Rumeysa Bulut. All rights reserved.
//

#include <cmath>
#include <iostream>
#include "HeapSort.hpp"
using namespace std;
void HeapSort::maxHeapify(long i){
    long size = heap_size;
    cout << size << endl;
    long l = 2*i + 1;   //index starts from 0
    long r = 2*i + 2;
    long temp;
    largest = i;
    
    if(l <= size - 1 && heap_array[l] > heap_array[i]){
        largest = l;
    }
    else{
        largest = i;
    }
    if(r <= size - 1 && heap_array[r] > heap_array[largest]){
        largest = r;
    }
    if(largest != i){
        temp = heap_array[i];
        heap_array[i] = heap_array[largest];
        heap_array[largest] = temp;
        maxHeapify(largest);
    }
}
void HeapSort::buildMaxHeap(){
    for(long i = floor(heap_size/2) - 1; i >= 0; i--){
        maxHeapify(i);
    }
}
void HeapSort::heapSort(){
    heap_size = heap_array.size();
    cout<< heap_size << endl;
    buildMaxHeap();
    long temp;
    for(long i = heap_array.size(); i >= 1; i--){
        temp = heap_array[0];
        heap_array[0] = heap_array[i-1];
        heap_array[i-1] = temp;
        heap_size = heap_size - 1;
        //cout << heap_size << endl;
        maxHeapify(0);
    }
}
void HeapSort::addElements(long j){
    heap_array.push_back(j);
}
long HeapSort::getSize(){
    return heap_array.size();
}
long HeapSort::getElement(long i){
    return heap_array[i];
}
